# coding=utf-8


#######################################################################################################################
#######################################################################################################################
#######################################################################################################################


import pandas as pd
import os
from config import GLOBAL_PATH, timeit


#######################################################################################################################
#######################################################################################################################
#######################################################################################################################


def check_files(files_folder=GLOBAL_PATH+'/data/raw/'):
    """
    This function simply return files path contained in a folder
    :param files_folder: folder containing raw data
    :return: list of string
    """
    files = os.listdir(files_folder)
    if '.DS_Store' in files:
        files.remove('.DS_Store')
    return [files_folder+f for f in files]


@timeit
def read_txt():
    """
    This function is specific to our data
    it parses all .txt contained in raw data folders and build a single dataframe
    :return: Pandas df
    """
    files = check_files()
    df_list = []
    for file in files:
        year = file.split('/')[-1].split('.txt')[0][-4:]
        df_list.append(pd.read_csv(file, header=None).assign(year=int(year)))
    big_df = pd.concat(df_list, ignore_index=True)
    big_df['name'] = big_df[0]
    big_df['gender'] = big_df[1]
    df = big_df[['name','gender','year']]
    return df


def filter_year(df, year=1900):
    """
    This function enables to filter data with year parameter
    :param year: int - year threshold
    :param df: Pandas df
    :return: Pandas df
    """
    df = df[df['year'] > year]
    return df


def remove_accent_and_lower(df):
    """
    This function removes any kind of accent in string
    :param df: Pandas df
    :return: Pandas df
    """
    df['name'] = df['name'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8').str.lower()
    return df


def create_dataset(year=None, remove_accent_lower=True):
    """

    :param year:
    :param remove_accent_lower:
    :return:
    """
    df = read_txt()
    if year is None:
        df = filter_year(df)
    else:
        df = filter_year(year)
    if remove_accent_lower:
        df = remove_accent_and_lower(df)
    return df

