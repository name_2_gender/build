# coding=utf-8


#######################################################################################################################
#######################################################################################################################
#######################################################################################################################


from models import first_last_letter_transformer_decision_tree_estimator_pipeline
from models import first_last_letter_transformer_gaussian_naive_bayes_estimator_pipeline
from models import all_letters_transformer_decision_tree_estimator_pipeline
from models import all_letters_transformer_gaussian_naive_bayes_estimator_pipeline
from preprocess import create_train_test_set
import read_filter as rf
from sklearn.metrics import accuracy_score
from sklearn.externals import joblib
from config import mlflow_uri, GLOBAL_PATH, check_last_index
import mlflow
import pandas as pd


#######################################################################################################################
#######################################################################################################################
#######################################################################################################################


def first_last_letter_decision_tree(x, y):
    """
    This function generates a model fitted on training set
    :param x: train set (not pre-processed)
    :param y: train labels
    :return: model - Sklearn objects
    """
    model = first_last_letter_transformer_decision_tree_estimator_pipeline()
    model.fit(x, y)
    return model


def first_last_letter_transformer_gaussian_nb(x, y):
    """
    This function generates a model fitted on training set
    :param x: train set (not pre-processed)
    :param y: train labels
    :return: model - Sklearn objects
    """
    model = first_last_letter_transformer_gaussian_naive_bayes_estimator_pipeline()
    model.fit(x, y)
    return model


def all_letters_decision_tree(x, y):
    """
    This function generates a model fitted on training set
    :param x: train set (not pre-processed)
    :param y: train labels
    :return: model - Sklearn objects
    """
    model = all_letters_transformer_decision_tree_estimator_pipeline()
    model.fit(x, y)
    return model


def all_letters_transformer_gaussian_nb(x, y):
    """
    This function generates a model fitted on training set
    :param x: train set (not pre-processed)
    :param y: train labels
    :return: model - Sklearn objects
    """
    model = all_letters_transformer_gaussian_naive_bayes_estimator_pipeline()
    model.fit(x, y)
    return model


#######################################################################################################################
#######################################################################################################################
#######################################################################################################################


# Build 0
"""
models = [first_last_letter_decision_tree, first_last_letter_transformer_gaussian_nb]
split_rate = 0.95
"""

# Build 1
"""
models = [all_letters_decision_tree, all_letters_transformer_gaussian_nb]
split_rate = 0.95
"""

# Build 2
models = [all_letters_decision_tree, all_letters_transformer_gaussian_nb]
split_rate = 0.85


#######################################################################################################################
#######################################################################################################################
#######################################################################################################################


if __name__ == '__main__':

    # Data Preparation
    df = rf.create_dataset()
    # Save Dataset
    last_index_dataset = check_last_index(GLOBAL_PATH + "/data/dataset")
    df.to_csv(GLOBAL_PATH+"/data/dataset/dataset_"+str(last_index_dataset+1)+".csv")

    # Generation of training and testing sets
    x_train, x_test, y_train, y_test = create_train_test_set(df, split_rate=split_rate)
    # Save Train and Test Sets
    train_tuples = list(zip(x_train, y_train))
    test_tuples = list(zip(x_test, y_test))
    train_set = pd.DataFrame(train_tuples, columns=['name', 'gender'])
    test_set = pd.DataFrame(test_tuples, columns=['name', 'gender'])
    last_index_ttset = check_last_index(GLOBAL_PATH + '/data/train_test_set')
    train_set.to_csv(GLOBAL_PATH + '/data/train_test_set/trainset_v' + str(last_index_ttset + 1) + '.csv', header=False,
                     index=False)
    test_set.to_csv(GLOBAL_PATH + '/data/train_test_set/testset_v' + str(last_index_ttset + 1) + '.csv', header=False,
                    index=False)

    # Set ML Flow Server and experiment name
    mlflow.set_tracking_uri(mlflow_uri)
    mlflow.set_experiment("name-gender")

    # Save  infos
    run_infos = dict()
    run_infos['dataset_name'] = "dataset_" + str(last_index_dataset+1) + '.csv'
    run_infos['train_set_name'] = 'trainset_v_' + str(last_index_ttset + 1) + '.csv'
    run_infos['test_set_name'] = 'testset_v_' + str(last_index_ttset + 1) + '.csv'

    # One run for each model
    for _model in models:

        with mlflow.start_run(run_name='Build_2'):

            try:

                mlflow.set_tag("model", _model.__name__)

                # Change below line for new model try
                grid = _model(x_train, y_train)

                # Create accuracy : change below if another metric needed
                y_pred = grid.predict(x_test)
                acc = accuracy_score(y_test, y_pred)

                # Save model to FTP
                model_filename = GLOBAL_PATH + '/models/'+_model.__name__+'.jb'
                joblib.dump(grid.best_estimator_, model_filename)

                # Save params to file
                with open(GLOBAL_PATH + "/reports/"+_model.__name__+"_details.txt", "w") as f:
                    f.write(str(grid.best_estimator_.get_params()))

                # ML Flow Track params of pipeline
                for param in grid.best_params_.keys():
                    mlflow.log_param(param, grid.best_params_[param])
                mlflow.log_param("split_rate", split_rate)

                # ML Flow Track metrics of pipeline
                mlflow.log_metric('acc', acc)

                # ML Flow Track artifact
                mlflow.log_artifact(GLOBAL_PATH + "/reports/"+_model.__name__+"_details.txt")
                mlflow.log_artifact(model_filename)
                mlflow.log_artifact(
                    GLOBAL_PATH + '/data/train_test_set/trainset_v' + str(last_index_ttset + 1) + '.csv')
                mlflow.log_artifact(
                    GLOBAL_PATH + '/data/train_test_set/testset_v' + str(last_index_ttset + 1) + '.csv')

                # End ML Flow Run
                mlflow.end_run()

            except ConnectionError:

                # End ML Flow Run
                mlflow.end_run()

