# coding=utf-8


#######################################################################################################################
#######################################################################################################################
#######################################################################################################################


from preprocess import FirstLastLetterTransformer, AllLettersTransformer
from sklearn.pipeline import Pipeline
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.naive_bayes import GaussianNB


#######################################################################################################################
#######################################################################################################################
#######################################################################################################################


def first_last_letter_transformer_decision_tree_estimator_pipeline():
    """
    Defines a pipeline composed with First & Last letter Transformer
    and decision tree Estimator
    :return: GridSearchCV - Sklearn object
    """
    parameters = dict()
    parameters['first_last_letter__n_char'] = [1]
    pipeline = Pipeline(steps=[('first_last_letter', FirstLastLetterTransformer()),
                               ('model', DecisionTreeClassifier())])
    return GridSearchCV(pipeline, parameters, n_jobs=-1)


def first_last_letter_transformer_gaussian_naive_bayes_estimator_pipeline():
    """
    Defines a pipeline composed with First & Last letter Transformer
    and decision tree Estimator
    :return: GridSearchCV - Sklearn object
    """
    parameters = dict()
    parameters['first_last_letter__n_char'] = [1]
    pipeline = Pipeline(steps=[('first_last_letter', FirstLastLetterTransformer()), ('model', GaussianNB())])
    return GridSearchCV(pipeline, parameters, n_jobs=-1)


def all_letters_transformer_decision_tree_estimator_pipeline():
    """
    Defines a pipeline composed with First & Last letter Transformer
    and decision tree Estimator
    :return: GridSearchCV - Sklearn object
    """
    parameters = dict()
    parameters['all_letters__n_char'] = [1]
    pipeline = Pipeline(steps=[('all_letters', AllLettersTransformer()),
                               ('model', DecisionTreeClassifier())])
    return GridSearchCV(pipeline, parameters, n_jobs=-1)


def all_letters_transformer_gaussian_naive_bayes_estimator_pipeline():
    """
    Defines a pipeline composed with First & Last letter Transformer
    and decision tree Estimator
    :return: GridSearchCV - Sklearn object
    """
    parameters = dict()
    parameters['all_letters__n_char'] = [1]
    pipeline = Pipeline(steps=[('all_letters', AllLettersTransformer()), ('model', GaussianNB())])
    return GridSearchCV(pipeline, parameters, n_jobs=-1)

